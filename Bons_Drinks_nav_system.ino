#include <Wire.h>
#include <Servo.h>
#include <ServoEaser.h>
#include <QMC5883.h>
#include <RotaryEncoder.h>

// Define digital ports
#define ENCODER 7
#define SERVO 8
#define LED_BORESTE 9
#define LED_BOMBORDO 10

// Init Servo
Servo s;
int servoFrameMillis = 20;  // minimum time between servo updates
ServoEaser se;


// Init QMC
QMC5883 qq;
int addr          = 0x0D;   // 7BIT Sensor addr

// Init Encoder
RotaryEncoder e(A2, A3);
int newPos = 0;

bool play_mode    = false;  // ON / OFF
bool debugMode    = true;   // Imprime as variações de HDG

int proa_inicial  = 0;
int proa_atual    = 0;
int deriva        = 5;      // variação de hdg aceitável
int menor_deriva  = 0;
int maior_deriva  = 0;
int neutro        = 90;     // 90 = neutro do leme
int atuacao       = 25;     // correção aplicada no leme
int correcao      = 0;

int angulo        = 0;      // Utilizado na correção 0 >360
int dmg           = -22;    // DMG da localidade

void setup()
{
  Serial.begin(9600);

  // Inicia o servo
  s.attach(SERVO);
  s.write(neutro);
  //se.begin(s, servoFrameMillis); 
  //se.easeTo(neutro, 500);
  
  // Inicia o Encoder
  pinMode(ENCODER, INPUT);

  // Inicia o QMC
  qq.begin();
    
  // Inicia os Leds
  pinMode(LED_BORESTE, OUTPUT);         // Boreste
  pinMode(LED_BOMBORDO, OUTPUT);        // Bombordo

  proa_inicial = GetRumo();
  CalculaDeriva();
  
  if (debugMode)
  {
    Serial.println("Piloto automático iniciado");
    Serial.print("Manter proa: ");
    Serial.println(proa_inicial);
  }
}

void loop()
{
    SetaRumo(); 
    if (play_mode)
    {
      ComandaLeme();
    }
}

// Ajusta o rumo pelo Encoder
void SetaRumo()
{
  if (digitalRead(ENCODER) != 1)
  {
    if(play_mode == true)
    {
      if (debugMode)
      {
        Serial.println("");
        Serial.println("Piloto automático desligado");
      }
      play_mode = false;
    }
    else
    {
      if (debugMode)
      {
        Serial.println("");
        Serial.println("Piloto automático ligado");
      }
      play_mode = true;
    }
  }


  static int pos = 0;
  e.tick();
  int newPos = e.getPosition();
  if (pos != newPos) {
    int ajuste = 0;
  
    if(pos < newPos){
      ajuste = 1;
    }else{
      ajuste = -1;
    }
    
    int nova_proa = CorrigeAngulo(proa_inicial + ajuste);
    if (debugMode)
    {
      Serial.println();
      Serial.print("Ajuste de proa para: ");
      Serial.print(nova_proa);
    }
    proa_inicial = nova_proa;
    CalculaDeriva();
    pos = newPos;
  }
}

// Opera o servo de acordo com as leituras
void ComandaLeme()
{   
    proa_atual = GetRumo();
    if (debugMode)
    {
      Serial.println("");
      Serial.print("Atual: ");
      Serial.println(proa_atual);
      Serial.print("Definida: ");
      Serial.println(proa_inicial);
    }

    correcao = 0;
    if (proa_atual < menor_deriva)
    {
      // Curva à boreste
      if (debugMode)
      {
        Serial.println("Curva boreste");
      }
      digitalWrite(LED_BOMBORDO, LOW); 
      digitalWrite(LED_BORESTE, HIGH);
      correcao = neutro + atuacao;
    }
    else if (proa_atual > maior_deriva)
    {
      // Curva à bombordo
      if (debugMode)
      {
        Serial.println("Curva bombordo");
      }
      digitalWrite(LED_BORESTE, LOW);
      digitalWrite(LED_BOMBORDO, HIGH);
      correcao = neutro - atuacao;
    }
    else
    {
      if (debugMode)
      {
        Serial.println("Manter proa");
      }
      digitalWrite(LED_BOMBORDO, LOW);
      digitalWrite(LED_BORESTE, LOW);
      correcao = neutro;
    }
    //se.easeTo(correcao, 500);
    s.write(correcao);
}


// Lê o rumo
float GetRumo()
{
  int hdg;
  
  qq.calculate();
  hdg = CorrigeAngulo(qq.getHeadingDegree('z') + dmg);

  if (debugMode)
  {
    Serial.println("");
    Serial.print("Leitura de proa: ");
    Serial.println(hdg);
  }
  return hdg;
}

// Calcula o intervalo de proas aceitáveis para o rumo
void CalculaDeriva()
{
  menor_deriva = CorrigeAngulo(proa_inicial - deriva);
  maior_deriva = CorrigeAngulo(proa_inicial + deriva);
}

// Ajusta os ângulos para o intervalo 001 - 360
int CorrigeAngulo(int angulo)
{
  if(angulo < 001){
    angulo = 360 + angulo;
  }

  if(angulo > 360){
     angulo = angulo - 360;
  }

  return angulo;
}
